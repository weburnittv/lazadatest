<?php
$I = new AcceptanceTester($scenario);

$I->wantTo('Add article success');

$I->sendPOST('/article/create', array('article[title]' => 'I am going to test - ' . rand(10, 90),
    'article[slug]' => 'testing-slug-' . time(),
    'article[content]' => 'Testing content',
    'article[tags]' => 'Tech,Music',
), ['article[photo_file]' => __DIR__ . '/../_data/thumb.jpg']);
$I->seeResponseJsonMatchesJsonPath("$.article");
$articleHash = end($I->grabDataFromResponseByJsonPath("$.article.hash"));
$articleSlug = end($I->grabDataFromResponseByJsonPath("$.article.slug"));
$I->wantTo('Add article fail');

$I->sendPOST('/article/create', array('article[title]' => 'I am going to test - ' . rand(10, 90),
    'article[content]' => 'Testing content'
), ['article[photo_file]' => __DIR__ . '/../_data/thumb.jpg']);
$I->seeResponseJsonMatchesJsonPath("$.error");

$I->wantTo('Add tags to article');
$I->sendPOST('/article/' . $articleHash . '/tags', array('form[tags]' => 'Coding, stylist'));
$I->seeResponseJsonMatchesJsonPath("$.status");

$I->wantTo('Add tags');
$I->sendPOST('/add/tags', array('form[tags]' => 'Latest, Tags'));
$I->seeResponseJsonMatchesJsonPath("$.tags");

$I->wantTo('Get Article by slug');
$I->sendGET('/article/' . $articleSlug);
$I->seeResponseJsonMatchesJsonPath("$.article");