<?php

use PaulAan\OwnBundle\Form\Transformer\TagTransformer;

class TagTransformerTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {

    }

    // tests
    public function testTagTransformer()
    {
        $em = $this->tester->grabServiceFromContainer("doctrine")->getManager();
        $tagTrans = new TagTransformer($em);
        $transformed = $tagTrans->reverseTransform("Tech,Network,Programing");
        $this->assertEquals(3, count($transformed));
        $this->assertRegExp("/(Tech|Network|Programing)/", $tagTrans->transform($transformed));
    }

}