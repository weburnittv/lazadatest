<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 5/12/15
 * Time: 11:45 PM
 */

namespace PaulAan\OwnBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use PaulAan\OwnBundle\Form\Transformer\TagTransformer;

class TagTransformType extends AbstractType
{

    protected $transformer;

    public function __construct(TagTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer($this->transformer, true);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class' => 'PaulAan\\OwnBundle\\Entity\\Tag',
            'required' => true,
            'multiple' => true
        ));
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'tag_type';
    }

}