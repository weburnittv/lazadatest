<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 7/25/15
 * Time: 11:22 PM
 */


namespace PaulAan\OwnBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Expose;

/**
 * Base class for most of our entities. Livecycle callbacks should be implemented here, if possible
 * http://www.doctrine-project.org/docs/orm/2.0/en/reference/events.html#lifecycle-callbacks
 *
 * @ORM\MappedSuperclass
 */
abstract class MediaEntity extends BaseEntity
{

    public function getAbsolutePath($file = null)
    {
        return null === $file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath($file = null)
    {
        $dir = str_replace(DIRECTORY_SEPARATOR, '/', $this->getUploadDir());
        return null === $file ? '/' . $this->getUploadDir() : $dir . '/' . $file;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return getcwd() . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getMediaDir()
    {
        return __DIR__ . '/../../../../web';
    }

    protected function getUploadDir()
    {
        return 'mediafile' . DIRECTORY_SEPARATOR . $this->getKind() . DIRECTORY_SEPARATOR . $this->getHash();
    }

}
