<?php

namespace PaulAan\OwnBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PaulAan\OwnBundle\Entity\ArticleRepository")
 */
class Article extends MediaEntity implements ContentCDNInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $photo;

    /**
     * @Assert\File(maxSize="2024k", mimeTypes={"image/jpeg", "image/png", "image/bmp", "image/gif"})
     */
    protected $photoFile;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", indexBy="name", fetch="EAGER")
     * @ORM\JoinTable(name="article_tags",
     *      joinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     * */
    private $tags;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct();
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Article
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @todo get object alias key
     * @return string
     */
    public function getKind()
    {
        return 'article';
    }

    public function uploadFile($adapter)
    {
        // TODO: Implement uploadFile() method.
    }

    public function deleteFile($adapter)
    {
        // TODO: Implement deleteFile() method.
    }

    public function getFile()
    {
        return $this->getAbsolutePath($this->photo);
    }

    public function getTag($name)
    {
        if (!isset($this->tags[$name])) {
            throw new \InvalidArgumentException("This tag is not added in this article.");
        }

        return $this->tags[$name];
    }

    /**
     * Add tags
     *
     * @param \PaulAan\OwnBundle\Entity\Tag $tags
     * @return Article
     */
    public function addTag(\PaulAan\OwnBundle\Entity\Tag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \PaulAan\OwnBundle\Entity\Tag $tags
     */
    public function removeTag(\PaulAan\OwnBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function setPhotoFile(\Symfony\Component\HttpFoundation\File\UploadedFile $photoFile)
    {
        $this->photoFile = $photoFile;
        $this->photo = str_replace(" ", "_", $photoFile->getClientOriginalName());
        try {
            if (is_object($this->photoFile)) {
                $this->photoFile->move($this->getUploadDir(), $this->photo);
                $this->photoFile = null;
            }
        } catch (\Exception $e) {

        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhotoFile()
    {
        return $this->photoFile;
    }
}
