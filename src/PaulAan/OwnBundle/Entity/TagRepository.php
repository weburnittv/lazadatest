<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 6/24/15
 * Time: 11:30 PM
 */

namespace PaulAan\OwnBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TagRepository extends EntityRepository
{

    public function searchTags($value)
    {
        $tagNames = array_filter(explode(",", $value));
        array_walk($tagNames, function ($item) {
            return trim($item);
        });
        $qb = $this->createQueryBuilder("tag");
        $qb->where($qb->expr()->in("tag.name", $tagNames));
        $tags = $qb->getQuery()->getResult();

        return $tags;
    }
}