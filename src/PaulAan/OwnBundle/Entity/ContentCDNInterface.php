<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 7/25/15
 * Time: 11:04 PM
 */


namespace PaulAan\OwnBundle\Entity;

interface ContentCDNInterface
{

    public function uploadFile($adapter);

    public function deleteFile($adapter);

    public function getFile();
}