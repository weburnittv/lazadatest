<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 6/25/15
 * Time: 1:34 AM
 */

namespace PaulAan\OwnBundle;


final class BackendEvents
{
    const DELETE_ARTICLE = 'delete.article';
    const UPDATE_ARTICLE = 'update.article';
    const DELETE_FILE = 'delete:file';
    const NEW_ARTICLE = 'new.article';
}