<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 6/25/15
 * Time: 10:42 AM
 */

namespace PaulAan\OwnBundle\Tests\Command;

use PaulAan\OwnBundle\Entity\Article;
use PaulAan\OwnBundle\Event\Backend\ArticleNewEvent;
use PaulAan\OwnBundle\EventListener\BackendEventListener;
use Symfony\Component\DependencyInjection\Container;
use Mockery as m;

class BackendEventListenerTest extends \PHPUnit_Framework_TestCase
{
    public function tearDown()
    {
        return m::close();
    }

    public function testOnArticleCreated()
    {
        $title = "Testing within mockery";
        $article = new Article();
        $output = m::mock('\Symfony\Component\Console\Output\OutputInterface')
            ->shouldReceive('writeln')
            ->once()
            ->andReturnUsing(function ($message) use ($title) {
                if ($message == 'New article has been created: ' . $title)
                    return true;
                else $this->assertEquals(true, false);
            })
            ->getMock();
        $container = m::mock(new Container());
        $event = m::mock('\PaulAan\OwnBundle\Event\Backend\ArticleNewEvent')
            ->shouldReceive('getContainer')
            ->andReturn($container)
            ->getMock();
        $event->shouldReceive('getOutput')
            ->once()
            ->andReturn($output)
            ->getMock();
        $event->shouldReceive('getArticle')
            ->once()
            ->andReturn($article->setTitle($title))
            ->getMock();

        $listener = new BackendEventListener();
        $listener->onNewArticle($event);
    }

}