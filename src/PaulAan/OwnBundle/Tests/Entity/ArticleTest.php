<?php

namespace PaulAan\OwnBundle\Tests\Entity;

use PaulAan\OwnBundle\Entity\Article;

class ArticleTest extends \PHPUnit_Framework_TestCase
{

    public function testGetTitle()
    {
        $article = new Article();
        $article->setTitle("Paul is writing test");
        $this->assertEquals("Paul is writing test", $article->getTitle());
    }
}