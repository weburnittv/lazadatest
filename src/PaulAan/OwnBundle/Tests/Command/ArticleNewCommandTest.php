<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 6/25/15
 * Time: 10:42 AM
 */

namespace PaulAan\OwnBundle\Tests\Command;

use PaulAan\OwnBundle\Command\ArticleNewCommand;
use PaulAan\OwnBundle\Event\Backend\ArticleNewEvent;
use PaulAan\OwnBundle\FrontendEvents;
use Mockery as m;
use Symfony\Component\Console\Tester\CommandTester;

class ArticleNewCommandTest extends \PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testExecute()
    {
        $tester = new CommandTester(new ArticleNewCommand());
        $tester->execute(array('event' => FrontendEvents::ARTICLE_CREATE_NEW, 'data' => 1, 'class' => 'PaulAan\\OwnBundle\\Entity\\Article'));
        $this;
    }
}