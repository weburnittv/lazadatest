<?php

namespace PaulAan\OwnBundle\Tests\Consumers;

use OldSound\RabbitMqBundle\RabbitMq\Consumer;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PaulAan\OwnBundle\Consumers\DistributeBackendConsumer;
use PaulAan\OwnBundle\FrontendEvents;
use PhpAmqpLib\Message\AMQPMessage;
use Mockery as m;
use Symfony\Bundle\FrameworkBundle\Tests\Functional\app\AppKernel;

class DistributeBackendConsumerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Check if the message is requeued or not correctly.
     *
     * @dataProvider processMessageProvider
     */
    public function testProcessMessage($processFlag, $expectedMethod, $expectedRequeue = null)
    {
        $amqpConnection = m::mock('\PhpAmqpLib\Connection\AMQPConnection')
            ->shouldIgnoreMissing();

        $amqpChannel = m::mock('\PhpAmqpLib\Channel\AMQPChannel')
            ->shouldIgnoreMissing();

        $consumer = new Consumer($amqpConnection, $amqpChannel);

        $callbackFunction = function () use ($processFlag) {
            return new DistributeBackendConsumer('dev');
        }; // Create a callback function with a return value set by the data provider.
        $consumer->setCallback($callbackFunction);

        // Create a default message
        $amqpMessage = new AMQPMessage(json_encode(array('data' => 1, 'class' => 'PaulAan\\OwnBundle\\Entity\\Article', 'event' => FrontendEvents::ARTICLE_CREATE_NEW)));
        $amqpMessage->delivery_info['channel'] = $amqpChannel;
        $amqpMessage->delivery_info['delivery_tag'] = 0;

        $amqpChannel
            ->shouldReceive('basic_reject')
            ->andReturn($this->returnCallback(function ($delivery_tag, $requeue) use ($expectedMethod, $expectedRequeue) {
                \PHPUnit_Framework_Assert::assertSame($expectedMethod, 'basic_reject'); // Check if this function should be called.
                \PHPUnit_Framework_Assert::assertSame($requeue, $expectedRequeue); // Check if the message should be requeued.
            }));

        $amqpChannel
            ->shouldReceive('basic_ack')
            ->andReturn($this->returnCallback(function ($delivery_tag) use ($expectedMethod) {
                \PHPUnit_Framework_Assert::assertSame($expectedMethod, 'basic_ack'); // Check if this function should be called.
            }));

        $consumer->processMessage($amqpMessage);
    }

    public function processMessageProvider()
    {
        return array(
            array(ConsumerInterface::MSG_ACK, 'basic_ack'), // Remove message from queue only if callback return not false
            array(ConsumerInterface::MSG_REJECT, 'basic_reject', false), // Reject and drop
        );
    }
}