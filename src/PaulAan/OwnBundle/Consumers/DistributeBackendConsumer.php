<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 7/25/15
 * Time: 9:09 PM
 */

namespace PaulAan\OwnBundle\Consumers;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class DistributeBackendConsumer distribute backend processing
 * @package TedmateRabbitBundle\Consumers
 */
class DistributeBackendConsumer implements ConsumerInterface
{
    private $env;

    public function __construct($env)
    {
        $this->env = $env;
    }

    public function execute(AMQPMessage $msg)
    {
        try {
            $data = json_decode($msg->body, true);
        } catch (\Exception $e) {
            return false;
        }

        $env = array(1 => 'dev', 0 => 'prod');

        if (count($data) < 3)
            return true;
        $command = $data['event'];

        $cmd = 'php ' . getcwd() . '/app/console ' . $command . ' --env=' . $env[$this->env];

        unset($data['event']);

        foreach ($data as $key => $value) {
            $cmd .= ' --' . $key . '=\'' . $value . '\'';
        }

        $commandProcess = new \Symfony\Component\Process\Process($cmd);
        $commandProcess->run();

        echo $commandProcess->getOutput();
    }
}