<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 7/25/15
 * Time: 11:54 PM
 */

namespace PaulAan\OwnBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Bridge\Monolog\Logger;
use PaulAan\OwnBundle\Event\BackendEvent;

abstract class AbstractBackendCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName($this->getTargetFrontendEvent())
            ->setDescription('Process Backend for frontend event:' . $this->getTargetFrontendEvent())
            ->addOption('data', null, InputOption::VALUE_REQUIRED, 'Object ID')
            ->addOption('class', null, InputOption::VALUE_REQUIRED, 'Object Class')
            ->addOption('target', null, InputOption::VALUE_OPTIONAL, 'Target Class ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $id = $input->getOption('data');
        $class = $input->getOption('class');

        $em = $container->get('doctrine')->getManager();

        $object = $em->getRepository($class)->find($id);
        if (!$object) {
            $output->writeln("<error>Object not found</error>");
            return true;
        }

        try {

            $dispatch = $container->get('event_dispatcher');
            $event = $this->getBackendEvent();

            if (!($event instanceof BackendEvent))
                throw new \Exception("You event has to extend PaulAan\\OwnBundle\\Event\\BackendEvent");

            $event->setContainer($container)
                ->setData($object);
            if ($target = $input->getOption("target"))
                $event->setTarget($target);

            $event->setOutput($output);

            $event = $dispatch->dispatch($event->getBackendEventSubscribe(), $event);

            $this->doAfter($container, $output);

        } catch (\Exception $e) {
            $producer = $this->getContainer()->get("old_sound_rabbit_mq.error_events_producer");
            $producer->publish(json_encode(array('data' => $input->getOption('data'), 'class' => $input->getOption('class'), 'event' => $this->getTargetFrontendEvent(), "error" => $e->getMessage())));
            $output->writeln("<error>This event can not be process:" . $e->getMessage() . "</error>");
            throw new \RuntimeException($e->getMessage());
        }
    }

    /**
     * @return BackendEvent
     */
    protected abstract function getBackendEvent();

    protected abstract function getTargetFrontendEvent();

    /**
     * override this function to do something right after finish execution within container and outputWriter from parameter
     */
    protected function doAfter(Container $container, OutputInterface $output)
    {

    }
}