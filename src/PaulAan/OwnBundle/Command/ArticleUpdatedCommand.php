<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 6/25/15
 * Time: 1:48 AM
 */

namespace PaulAan\OwnBundle\Command;


use PaulAan\OwnBundle\Event\Backend\ArticleUpdatedEvent;
use PaulAan\OwnBundle\Event\BackendEvent;
use PaulAan\OwnBundle\FrontendEvents;

class ArticleUpdatedCommand extends AbstractBackendCommand
{

    /**
     * @return BackendEvent
     */
    protected function getBackendEvent()
    {
        return new ArticleUpdatedEvent();
    }

    protected function getTargetFrontendEvent()
    {
        return FrontendEvents::ARTICLE_UPDATE_CONTENT;
    }
}