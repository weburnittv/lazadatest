<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 6/25/15
 * Time: 10:42 AM
 */

namespace PaulAan\OwnBundle\Command;

use PaulAan\OwnBundle\Event\Backend\ArticleNewEvent;
use PaulAan\OwnBundle\FrontendEvents;

class ArticleNewCommand extends AbstractBackendCommand
{

    /**
     * @return BackendEvent
     */
    protected function getBackendEvent()
    {
        return new ArticleNewEvent();
    }

    protected function getTargetFrontendEvent()
    {
        return FrontendEvents::ARTICLE_CREATE_NEW;
    }
}