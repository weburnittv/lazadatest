<?php

namespace PaulAan\OwnBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PaulAanOwnBundle:Default:index.html.twig', array('name' => $name));
    }
}
