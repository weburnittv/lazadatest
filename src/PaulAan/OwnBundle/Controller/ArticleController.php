<?php

namespace PaulAan\OwnBundle\Controller;

use PaulAan\OwnBundle\Entity\Article;
use PaulAan\OwnBundle\Event\Frontend\ArticleCreatedEvent;
use PaulAan\OwnBundle\Event\Frontend\ArticleUpdatedEvent;
use PaulAan\OwnBundle\Form\ArticleType;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use PaulAan\OwnBundle\FrontendEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\HttpCacheBundle\Configuration\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ArticleController extends Controller
{
    /**
     * @ApiDoc(
     * section="Article",
     * description="Create Article",
     * parameters={
     *      {
     *          "name"="article[photo_file]",
     *          "dataType"="file",
     *          "description"="thumbnail",
     *          "required"="true"
     *      }
     *  },
     *  filters={
     *      {"name"="article[content]", "dataType"="string", "description"="content", "method"="POST"},
     *      {"name"="article[slug]", "dataType"="string", "description"="slug", "pattern"="YYYY-MM-DD", "method"="POST"},
     *      {"name"="article[title]", "dataType"="string", "description"="title", "method"="POST"}
     * }
     * )
     * @Rest\View(serializerGroups={"Default"},statusCode=200)
     * @Rest\Post("/article/create")
     */
    public function newAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm(new ArticleType(), $article);

        $form->submit($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
            $dispatch = $this->get("event_dispatcher");
            $event = new ArticleCreatedEvent($article);
            $dispatch->dispatch(FrontendEvents::ARTICLE_CREATE_NEW, $event);

        } else return $this->view(array('error' => $form->getErrors(true)), Response::HTTP_BAD_REQUEST);
        return array('article' => $article);
    }

    /**
     * @ApiDoc(
     * section="Article",
     * description="Add tag for post",
     *  filters={
     *      {"name"="form[tags]", "dataType"="string", "description"="Tag name", "method"="POST"}
     * }
     * )
     * @Rest\View(serializerGroups={"Default"},statusCode=200)
     * @Rest\Post("/article/{hash}/tags")
     * @Template()
     */
    public function addPostTagsAction(Request $request, $hash)
    {
        $form = $this->createFormBuilder(null, array('csrf_protection' => false))->add("tags", "tag_type", array('required' => true))->getForm();
        $form->submit($request);
        if ($form->isValid()) {
            $data = $form->getData();
        } else
            return $this->view(array('error' => $form->getErrors(true)));

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository("PaulAanOwnBundle:Article")->findOneByHash($hash);

        if (!$article)
            return $this->view(array('error' => "Object not found"), Response::HTTP_NOT_FOUND);
        if (isset($data['tags'])) {
            foreach ($data['tags'] as $tag)
                $article->addTag($tag);


            $em->persist($article);
            $em->flush();

            $dispatch = $this->get("event_dispatcher");
            $event = new ArticleUpdatedEvent($article);
            $dispatch->dispatch(FrontendEvents::ARTICLE_UPDATE_CONTENT, $event);

            $cache = $this->get("fos_http_cache.cache_manager");
            $cache->invalidateTags(array('article-' . $article->getSlug()))//Ban
            ->invalidateRoute("show", array("slug" => $article->getSlug()))//Purge
            ->flush();

        } else return $this->view(array("error" => "Tag must be at least one term"), Response::HTTP_EXPECTATION_FAILED);
        return array("status" => true);
    }

    /**
     * @ApiDoc(
     * section="Article",
     * description="Add tags",
     *  filters={
     *      {"name"="form[tags]", "dataType"="string", "description"="Tag name", "method"="POST"}
     * }
     * )
     * @Rest\View(serializerGroups={"Default"},statusCode=200)
     * @Rest\Post("/add/tags")
     */
    public function addTagsAction(Request $request)
    {
        $form = $this->createFormBuilder(null, array('csrf_protection' => false))->add("tags", "tag_type", array('required' => true))->getForm();
        $form->submit($request);
        if ($form->isValid()) {
            $data = $form->getData();
            return array('tags' => $data);
        } else
            return $this->view(array('error' => $form->getErrors(true)));
    }

    /**
     * Show Artcile
     * @ApiDoc(
     * section="Article",
     * description="Show article"
     * )
     * @Rest\View(serializerGroups={"Detail", "Default"},statusCode=200)
     * @ParamConverter("article", class="PaulAanOwnBundle:Article", options={"mapping":{"slug":"slug"}})
     * @Tag(expression="'article-'~article.getSlug")
     * @Cache(public=true, lastModified="article.getUpdatedAt()")
     * @Rest\Get("/article/{slug}")
     */
    public function showAction(Article $article)
    {
        return array('article' => $article);
    }

}
