<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 6/25/15
 * Time: 1:34 AM
 */

namespace PaulAan\OwnBundle;


final class FrontendEvents
{
    const ARTICLE_CREATE_NEW = 'article:create:new';
    const ARTICLE_UPDATE_CONTENT = 'article:update:content';
    const ARTICLE_DELETE = 'article:delete';
}