<?php

namespace PaulAan\OwnBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use PaulAan\OwnBundle\FrontendEvents;
use PaulAan\OwnBundle\Event\FrontendEvent;
use OldSound\RabbitMqBundle\RabbitMq\Producer;

class FrontendEventListener implements EventSubscriberInterface
{
    /**
     * @var Producer
     */
    private $producer;

    public function __construct(Producer $producer)
    {
        $this->producer = $producer;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FrontendEvents::ARTICLE_CREATE_NEW => 'publishBackendJob'
        );
    }

    public function publishBackendJob(FrontendEvent $event)
    {
        $data = $event->getEventData();
        $data['event'] = $event->getName();
        $this->producer->publish(json_encode($data));
    }

}
