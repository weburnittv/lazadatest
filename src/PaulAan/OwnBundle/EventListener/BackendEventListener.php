<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 3/20/15
 * Time: 4:04 PM
 */

namespace PaulAan\OwnBundle\EventListener;

use PaulAan\OwnBundle\Event\Backend\ArticleNewEvent;
use PaulAan\OwnBundle\Event\Backend\ArticleUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\Container;
use PaulAan\OwnBundle\FrontendEvents;
use PaulAan\OwnBundle\BackendEvents;

class BackendEventListener implements EventSubscriberInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param Container $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
        return $this;
    }


    public static function getSubscribedEvents()
    {
        return array(
            BackendEvents::NEW_ARTICLE => 'onNewArticle',
            BackendEvents::UPDATE_ARTICLE => 'onUpdateArticle'
        );
    }

    public function onUpdateArticle(ArticleUpdatedEvent $event)
    {
        $this->setContainer($event->getContainer());
        $article = $event->getArticle();

        $output = $event->getOutput();
        $output->writeln("Article has been updated: " . $article->getTitle());
    }

    public function onNewArticle(ArticleNewEvent $event)
    {
        $this->setContainer($event->getContainer());
        $article = $event->getArticle();

        $output = $event->getOutput();
        $output->writeln("New article has been created: " . $article->getTitle());
    }


    private function flushData()
    {
        $em = $this->getEntityManager();

        $em->getConnection()->beginTransaction();
        try {
            $em->flush();
            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            $em->close();
            throw new \RuntimeException("An error occurs:" . $e->getMessage());
        }
    }

    private function getEntityManager()
    {
        if (!$this->em)
            $this->em = $this->container->get("doctrine")->getManager();
        return $this->em;
    }

}