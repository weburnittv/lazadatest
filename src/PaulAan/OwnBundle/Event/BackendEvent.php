<?php

namespace PaulAan\OwnBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BackendEvent extends Event
{
    /**
     * @var Container
     */
    private $container;
    protected $target;

    /**
     * @var OutputInterface
     */
    private $output;

    protected $data;

    function __construct($data = null)
    {
        if ($data) $this->data = $data;
    }

    /**
     * @return Container
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param null $data
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
        $this->output->writeln("Start running event:" . $this->getBackendEventSubscribe());
        return $this;
    }

    public function setTarget($id)
    {
        $this->target = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    public abstract function getBackendEventSubscribe();

}