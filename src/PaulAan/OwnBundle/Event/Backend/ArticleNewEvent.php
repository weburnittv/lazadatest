<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 6/25/15
 * Time: 10:43 AM
 */

namespace PaulAan\OwnBundle\Event\Backend;
use PaulAan\OwnBundle\BackendEvents;

class ArticleNewEvent extends ArticleUpdatedEvent{

    public function getBackendEventSubscribe()
    {
        return BackendEvents::NEW_ARTICLE;
    }
}