<?php

namespace PaulAan\OwnBundle\Event\Backend;

use PaulAan\OwnBundle\BackendEvents;
use PaulAan\OwnBundle\Event\BackendEvent;
use PaulAan\OwnBundle\Entity\Article;

class ArticleUpdatedEvent extends BackendEvent
{

    /**
     * @return Article
     */
    public function getArticle()
    {
        return $this->data;
    }


    public function getBackendEventSubscribe()
    {
        return BackendEvents::UPDATE_ARTICLE;
    }
}