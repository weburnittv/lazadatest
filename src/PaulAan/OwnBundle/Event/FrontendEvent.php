<?php

/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 7/25/15
 * Time: 11:15 PM
 */


/**
 * Description of InvitationEvent
 *
 * @author paulaan
 */

namespace PaulAan\OwnBundle\Event;

use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\EventDispatcher\Event;

abstract class FrontendEvent extends Event
{

    protected $object;

    public function __construct($object = null)
    {
        if ($object)
            $this->object = $object;
    }

    public function getObject()
    {
        return $this->object;
    }

    public function setObject($object)
    {
        $this->object = $object;
    }

    public function getTarget()
    {
        return $this->object;
    }

    public static function getClass($class)
    {
        return ClassUtils::getClass($class);
    }
    /**
     * @return mixed event data
     */
    public abstract function getEventData();

}
