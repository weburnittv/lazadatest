<?php
/**
 * Created by PhpStorm.
 * User: paulaan
 * Date: 6/25/15
 * Time: 1:52 AM
 */

namespace PaulAan\OwnBundle\Event\Frontend;

use PaulAan\OwnBundle\Entity\Article;
use PaulAan\OwnBundle\Event\FrontendEvent;

class ArticleUpdatedEvent extends FrontendEvent
{

    /**
     * @var Article
     */
    protected $article;

    function __construct(Article $article)
    {
        $this->article = $article;
    }


    /**
     * @return mixed event data
     */
    public function getEventData()
    {
        return array('data' => $this->article->getId(), 'class' => parent::getClass($this->article));
    }
}