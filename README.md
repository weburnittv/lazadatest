Lazada Test
===========

A Test project created on June 24, 2015, 8:36 pm.

Pre-requisite
============
**Install Rabbitmq** [https://www.rabbitmq.com/download.html](https://www.rabbitmq.com/download.html)

**Install memcached extension** [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-memcache-on-ubuntu-12-04](php-memcached)

**Install composer** [http://getcomposer.org/](http://getcomposer.org/)

**Update/Install environments**

    > composer install

**Run migration db**

    > app/console d:d:c
    > app/console d:m:m
    
Run Test
=======

```ssh
> bin/codeception build
```

```ssh
> bin/codeception run --steps
```

```ssh
> bin/phpunit -c app/
```
